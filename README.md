# Website

This repository contains files to generate my personal website [Nayab Basha Sayed](https://www.nayab.xyz).

[jekyll](https://jekyllrb.com) - This is the static site generator used to build my personal website.

I use [Netlify](https://www.netlify.com) to host my personal website and [Netlify CMS](https://www.netlifycms.org) to write or manage my posts.
